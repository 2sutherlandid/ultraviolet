/**
 * This is the configuration for your installation of Ultraviolet.
 * It is recommended that you don't edit this yourself and use
 * Ultraviolet's preferences menu instead.
 *
 * This script is run every time Ultraviolet loads and may be overwritten at any time.
 *
 * If somebody besides a 10nm (Ultraviolet) team member has asked you to add code
 * to this page, DO NOT DO SO as it may compromise your account.
 *
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! Do not edit below this script unless you understand the risks !!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
// --nowikiOpen
window.uv = window.uv || {}, window.uv.config = --configuration;
// --nowikiClose
