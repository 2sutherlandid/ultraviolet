/**
 *
 * Ultraviolet - the user-friendly MediaWiki counter-vandalism tool.
 *
 * (c) 2020-2022 The Ultraviolet Development Team (10nm) and contributors - tools.ultraviolet@toolforge.org or [[WT:UV]]
 * Licensed under the Apache License 2.0 - read more at https://gitlab.com/redwarn/ultraviolet/
 * Other conditions may apply - check LICENSE for more information.
 *
 **/

/* Libraries */

import i18next from "i18next";
import * as AppConstants from "./data/UltravioletConstants";
import { APP_VERSION } from "./data/UltravioletConstants";
import * as Utilities from "./util";
import { getCircularReplacer } from "./util";
import Dependencies from "./data/Dependencies";
import Localization from "./localization/Localization";
import Log from "app/data/AppLog";
import MediaWikiNotificationContent from "app/ui/MediaWikiNotificationContent";
import RealTimeRecentChanges from "./integrations/RealTimeRecentChanges";
import UltravioletHooks from "./event/UltravioletHooks";
import UltravioletLocalDB from "app/data/database/UltravioletLocalDB";
import UltravioletStore from "./data/UltravioletStore";
import UltravioletUI from "./ui/UltravioletUI";
import UltravioletWikiConfiguration from "app/config/wiki/UltravioletWikiConfiguration";
import StyleManager from "./styles/StyleManager";
import TamperProtection from "./tamper/TamperProtection";
import UIInjectors from "app/ui/injectors/UIInjectors";
import * as MediaWikiClasses from "./mediawiki";
import {
    ClientUser,
    MediaWiki,
    MediaWikiAPI,
    RevertSpeedup,
    WarningManager,
    Watch,
} from "./mediawiki";
import { Configuration } from "./config/user/Configuration";
import LoadErrorTranslations from "app/errors/LoadErrorTranslations";
import { RecentPages } from "app/mediawiki/util/RecentPages";

declare global {
    interface Window {
        Ultraviolet: typeof Ultraviolet;
        uv: typeof Ultraviolet;
        RedWarn: typeof Ultraviolet; // DO NOT USE: backcompat
        rw: typeof Ultraviolet; // DO NOT USE: backcompat
    }
}

export enum UltravioletLoadState {
    Preload,
    Load,
    Preinitialization,
    Initialization,
    Postinitialization,
    PreUIInjection,
    PostUIInjection,
    Ready,
    Failed,
    Deinitialization,
    Terminated,
}

/**
 * The Ultraviolet class is provided as a way for other scripts to access
 * Ultraviolet-specific functionality.
 */
export default class Ultraviolet {
    static readonly version = APP_VERSION;

    // TODO: Remove exposure of unused classes.

    static get UltravioletConstants(): typeof AppConstants {
        return AppConstants;
    }
    static get UltravioletStore(): typeof UltravioletStore {
        return UltravioletStore;
    }
    static get UltravioletHooks(): typeof UltravioletHooks {
        return UltravioletHooks;
    }
    static get Localization(): typeof Localization {
        return Localization;
    }
    static get Log(): typeof Log {
        return Log;
    }
    static get i18next(): typeof i18next {
        return i18next;
    }
    static get MediaWikiClasses(): typeof MediaWikiClasses {
        return MediaWikiClasses;
    }
    static get StyleManager(): typeof StyleManager {
        return StyleManager;
    }
    static get UltravioletUI(): typeof UltravioletUI {
        return UltravioletUI;
    }
    static get RTRC(): typeof RealTimeRecentChanges {
        return RealTimeRecentChanges;
    }
    static get Utilities(): typeof Utilities {
        return Utilities;
    }
    static get WarningManager(): typeof WarningManager {
        return WarningManager;
    }
    static get Dependencies(): typeof Dependencies {
        return Dependencies;
    }
    static get Configuration(): typeof Configuration {
        return Configuration;
    }
    static get WikiConfiguration(): typeof UltravioletWikiConfiguration {
        return UltravioletWikiConfiguration;
    }
    static get Database(): UltravioletLocalDB {
        return UltravioletLocalDB.i;
    }
    static get Watch(): typeof Watch {
        return Watch;
    }
    static get RecentPages(): typeof RecentPages {
        return RecentPages;
    }
    static get MediaWiki(): typeof MediaWiki {
        return MediaWiki;
    }
    static get ClientUser(): typeof ClientUser {
        return ClientUser;
    }
    static get TamperProtection(): typeof TamperProtection {
        return TamperProtection;
    }

    static loadState: UltravioletLoadState = UltravioletLoadState.Preload;
    static async initialize() {
        if (
            document.body.classList.contains("rw-disable") ||
            document.body.classList.contains("uv-disable") ||
            new URL(window.location.href).searchParams.get("redwarn") === "0" ||
            new URL(window.location.href).searchParams.get("ultraviolet") ===
                "0"
        ) {
            // We've been prevented from running on this page.
            Log.info(
                "The current page is blocking Ultraviolet from loading. Shutting down..."
            );
            return;
        }

        // Tell all scripts that Ultraviolet is running.
        UltravioletHooks.executeHooks("load");
        Ultraviolet.loadState = UltravioletLoadState.Load;

        Log.info(`Starting Ultraviolet ${APP_VERSION}...`);
        const startTime = Date.now();

        if (window.uv != null || window.rw != null) {
            mw.notify(
                "You have two versions of Ultraviolet/RedWarn installed at once! To prevent issues, please edit your common.js or skin.js files to ensure that you only use one version of the script.",
                { type: "error", title: "Conflict" }
            );
            return; // die
        }

        window.Ultraviolet = Ultraviolet;
        window.uv = Ultraviolet;

        // Initialize components here.
        // As much as possible, each component should be its own class to make everything
        // organized.

        // Hook into page errors and observe possible warnings.
        window.addEventListener("error", (event) => {
            if (
                event.filename.includes("ultraviolet") ||
                event.filename.includes("redwarn")
            ) {
                Log.error("Uncaught error: ", { event });
            }
        });

        // Load in languages first.
        await Localization.init();

        // Verify our MediaWiki installation.
        if (!MediaWiki.mwCheck()) return;

        // Load in MediaWiki dependencies
        await MediaWiki.loadDependencies();

        // Static dependency initialization.
        await Promise.all([
            // Connect to the Indexed DB database.
            UltravioletLocalDB.i.connect(),
            // Initialize the MediaWiki API.
            MediaWikiAPI.init(),
            (async () => {
                UltravioletStore.initializeStore();
            })(),
            StyleManager.initialize(),
        ]);

        try {
            // Attempt to deserialize the per-wiki configuration.
            UltravioletWikiConfiguration.c;
        } catch (e) {
            Log.fatal("Wiki-specific configuration is broken!");
            mw.notify(
                MediaWikiNotificationContent(
                    i18next.t(`mediawiki:error.wikiConfigBad`, {
                        wikiIndex: UltravioletStore.wikiIndex,
                    })
                ),
                { type: "error" }
            );
            return;
        }

        // Load in the configuration file (preloads need to be finished by this point).
        await Configuration.refresh();

        /**
         * Extensions and styles can push their own dependencies here.
         */
        await Promise.all([
            UltravioletHooks.executeHooks("preInit"),
            Dependencies.resolve([StyleManager.activeStyle.dependencies]),
            Dependencies.resolve([UltravioletStore.dependencies]),
        ]);
        Ultraviolet.loadState = UltravioletLoadState.Preinitialization;

        /**
         * Initialize everything
         */

        // Non-blocking initializers.
        (() => {
            RealTimeRecentChanges.init();
            TamperProtection.init();

            // Call RevertSpeedup last.
            RevertSpeedup.init();
        })();

        await Promise.all([UltravioletHooks.executeHooks("init")]);
        Ultraviolet.loadState = UltravioletLoadState.Initialization;

        /**
         * Send notice that Ultraviolet is done loading.
         */
        await UltravioletHooks.executeHooks("postInit");
        Ultraviolet.loadState = UltravioletLoadState.Postinitialization;
        Log.debug(`Done loading (core): ${Date.now() - startTime}ms.`);

        // Inject all UI elements
        await UltravioletHooks.executeHooks("preUIInject");
        Ultraviolet.loadState = UltravioletLoadState.PreUIInjection;

        await UIInjectors.i.inject();

        await Promise.all([
            UltravioletHooks.executeHooks("postUIInject"),
            Watch.init(),
            RecentPages.init(),
        ]);
        Ultraviolet.loadState = UltravioletLoadState.PostUIInjection;

        Log.debug(`Done loading (UI): ${Date.now() - startTime}ms.`);
        Ultraviolet.loadState = UltravioletLoadState.Ready;
    }

    static async start(): Promise<void> {
        return Ultraviolet.initialize().catch((e) => {
            // DO NOT USE i18next! i18next might not be able to load, causing the issue in the first place.
            Log.fatal("Error loading Ultraviolet!", e);

            const translated =
                LoadErrorTranslations[navigator.language] ?? // "zh-TW"
                LoadErrorTranslations[/^[A-Z]+/i.exec(navigator.language)[0]] ?? // "en"
                LoadErrorTranslations["en"]; // fallback

            const standardCSS =
                "background: #e0005a; color: #ffffff; font-weight: bold; font-size: x-large;";
            console.group(
                `%c${translated[0]}%c${translated.slice(1).join(" ")}`,
                `padding: 2px 8px; border-radius: 8px; ${standardCSS}`,
                "display: block; margin-top: 8px; width: 50vw;"
            );
            console.log(
                "%c" + btoa(JSON.stringify(Log.dump(), getCircularReplacer())),
                "color: lime;"
            );
            console.groupEnd();

            Ultraviolet.loadState = UltravioletLoadState.Failed;
        });
    }

    /**
     * Close down all Ultraviolet processes, calls cleanup functions of injectors and
     * styles, and prepares for removal from the page. All processes here only involve
     * those that have an effect on the DOM and Window - things such as UltravioletStore
     * is not deinitialized since they will be deleted by garbage collection once this
     * function ends.
     *
     * Does not reload dependencies. Re-running dependencies may lead to unexpected
     * outcomes.
     *
     * Highly experimental, may lead to instability.
     */
    static async deinitialize() {
        Log.warn("Deinitializing Ultraviolet. This is experimental!");

        if (Ultraviolet.loadState < UltravioletLoadState.Ready) {
            let checkInterval: any;
            await new Promise<void>((res) => {
                checkInterval = setInterval(() => {
                    if (Ultraviolet.loadState == UltravioletLoadState.Ready) {
                        clearInterval(checkInterval);
                        res();
                    }
                }, 100);
            });
        }
        Ultraviolet.loadState = UltravioletLoadState.Deinitialization;

        // Call all deinitialization hooks.
        await Promise.all([
            UltravioletHooks.executeHooks("deinit"),
            Watch.deinit(),
            Dependencies.unresolve(),
        ]);

        delete window.Ultraviolet;
        delete window.uv;

        Log.info("Ultraviolet terminated.");
        UltravioletHooks.executeHooks("terminate");
        Ultraviolet.loadState = UltravioletLoadState.Terminated;
    }
}

if ((module as any).hot) {
    Log.info(
        "Hot reload detected. Attempting to deinitialize existing Ultraviolet."
    );
    (module as any).hot.accept();
    if (window.uv)
        window.uv
            .deinitialize()
            .then(() => {
                Ultraviolet.start();
            })
            .catch((e) => {
                Log.fatal("Error deinitializing Ultraviolet!", e);
            });
    else Ultraviolet.start();
} else {
    Ultraviolet.start();
}
