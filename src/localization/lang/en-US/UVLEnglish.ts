import { APP_VERSION } from "app/data/UltravioletConstants";

if (window.UltravioletLanguages == null) {
    window.UltravioletLanguages = [];
}

window.UltravioletLanguages.push({
    tag: "en-US",
    id: "ultraviolet-default-en-US",
    meta: {
        name: "Ultraviolet English",
        translators: ["The Ultraviolet Contributors"],
        license: {
            url: "https://www.apache.org/licenses/LICENSE-2.0.txt",
            text: "Apache License 2.0",
        },
        version: APP_VERSION,
        links: {
            home: "https://en.wikipedia.org/wiki/Wikpedia:Ultraviolet",
            license:
                "https://gitlab.com/redwarn/ultraviolet/-/blob/master/LICENSE",
        },
    },
    namespaces: {
        common: require("./common.json"),
        mediawiki: require("./mediawiki.json"),
        ui: require("./ui.json"),
        misc: require("./misc.json"),
        prefs: require("./prefs.json"),
        revert: require("./revert.json"),
    },
});
