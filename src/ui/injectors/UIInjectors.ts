import DiffViewerInjector from "app/ui/injectors/DiffViewerInjector";
import ContributionsPageInjector from "app/ui/injectors/ContributionsPageInjector";
import PageIconsInjector from "app/ui/injectors/PageIconsInjector";
import PreferencesInjector from "./PreferencesInjector";
import { ClientUser } from "app/mediawiki";
import RenderedVisualsInjector from "app/ui/injectors/RenderedVisualsInjector";

export default class UIInjectors {
    private static instance: UIInjectors = new UIInjectors();
    static get i(): UIInjectors {
        return UIInjectors.instance;
    }

    pageIconsInjector: PageIconsInjector = new PageIconsInjector();
    renderedVisualsInjetor: RenderedVisualsInjector =
        new RenderedVisualsInjector();

    diffViewerInjector: DiffViewerInjector = new DiffViewerInjector();
    contributionsPageInjector: ContributionsPageInjector =
        new ContributionsPageInjector();
    preferencesInjector: PreferencesInjector = new PreferencesInjector();

    private constructor() {
        /* private constructor */
    }

    /**
     * Run all injectors.
     *
     * Injectors are responsible for modifying existing MediaWiki DOM. This allows
     * for non-invasive DOM procedures, and allows a separation between UI and
     * DOM-modifying code from actual API functionality.
     */
    async inject(): Promise<any> {
        const initPromises = [
            this.pageIconsInjector.init(),
            this.renderedVisualsInjetor.init(),
        ];
        // Check if the user is confirmed.
        if (ClientUser.i.inGroup("confirmed")) {
            initPromises.push(
                this.diffViewerInjector.init(),
                this.contributionsPageInjector.init(),
                this.preferencesInjector.init()
            );
        }

        return Promise.all(initPromises);
    }
}
