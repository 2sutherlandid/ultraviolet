import UltravioletStore from "app/data/UltravioletStore";
import i18next from "i18next";
import UltravioletUI from "app/ui/UltravioletUI";
import {
    ClientUser,
    groupFromName,
    Page,
    ProtectionManager,
    User,
    Watch,
} from "app/mediawiki";
import { copyToClipboard, redirect } from "app/util";
import UltravioletWikiConfiguration from "app/config/wiki/UltravioletWikiConfiguration";
import Log from "app/data/AppLog";
import { getReportVenueIcons } from "app/mediawiki/report/ReportVenue";

interface PageIconBase {
    icon: string;
    color?: string;
    /** Whether or not this icon is on the toolbar by default. */
    default?: boolean;
    /** Whether or not this icon is required on the toolbar. */
    required?: boolean;
    visible(): PromiseOrNot<boolean>;
    action(event: MouseEvent): Promise<any> | void;
}

export type PageIcon = PageIconBase &
    ({ id: string; name?: never } | { id?: string; name: string });

export const PageIcons = (): PageIcon[] => {
    if (!ClientUser.i.inGroup("confirmed")) {
        return [
            {
                id: "missingGroup",
                name: i18next.t("ui:missingGroup.label"),
                icon: "new_releases",
                color: "red",
                default: true,
                visible: () => true,
                action() {
                    new UltravioletUI.AlertDialog({
                        title: i18next.t("ui:missingGroup.label"),
                        content: i18next.t<string>(
                            "ui:missingGroup.description",
                            {
                                group: groupFromName("confirmed").displayName,
                            }
                        ),
                        actions: [
                            {
                                data: i18next.t("ui:ok"),
                            },
                        ],
                    }).show();
                },
            },
        ];
    }

    const defaultUserIcons = [
        {
            id: "message",
            icon: "send",
            default: true,
            visible: UltravioletStore.isUserspacePage,
            action() {
                User.relevantUser.openMessageDialog();
            },
        },
        {
            id: "quickTemplate",
            icon: "library_add",
            default: true,
            visible: UltravioletStore.isUserspacePage,
            action() {
                UltravioletUI.Toast.quickShow({
                    content: i18next.t("ui:unfinished"),
                });
            },
        },
        {
            id: "warn",
            icon: "report",
            default: true,
            visible: () =>
                UltravioletStore.isUserspacePage() &&
                UltravioletWikiConfiguration.c.warnings != null,
            async action() {
                const options = await new UltravioletUI.WarnDialog({
                    targetUser:
                        mw.config.get("wgRelevantUserName") &&
                        User.fromUsername(mw.config.get("wgRelevantUserName")),
                }).show();
                User.warn(options)
                    .then((v) => {
                        if (v) {
                            UltravioletUI.Toast.quickShow({
                                content: i18next.t("ui:toasts.userWarned"),
                                action: {
                                    text: i18next.t(
                                        "ui:toasts.userWarnedAction"
                                    ),
                                    callback: () => {
                                        options.targetUser.talkPage.navigate();
                                    },
                                },
                            });
                        }
                    })
                    .catch((e) => {
                        // TODO: Provide more details.
                        UltravioletUI.Toast.quickShow({
                            content: i18next.t("ui:toasts.userWarnFailed"),
                        });
                        Log.error("Failed to warn user.", e);
                    });
            },
        },
    ];

    const defaultIcons = [
        {
            id: "protection",
            icon: "lock",
            default: true,
            visible: () =>
                !UltravioletStore.isSpecialPage() &&
                UltravioletWikiConfiguration.c.protection?.duration
                    ?.temporary != null &&
                UltravioletWikiConfiguration.c.protection?.duration
                    ?.indefinite != null,
            async action() {
                const options =
                    await new UltravioletUI.ProtectionRequestDialog().show();
                ProtectionManager.requestProtection(options)
                    .then((v) => {
                        if (v) {
                            UltravioletUI.Toast.quickShow({
                                content: i18next.t(
                                    "ui:toasts.protectionRequested"
                                ),
                                action: {
                                    text: i18next.t("ui:toasts.viewAction"),
                                    callback: () => {
                                        v.navigate();
                                    },
                                },
                            });
                        }
                    })
                    .catch((e) => {
                        // TODO: Provide more details.
                        UltravioletUI.Toast.quickShow({
                            content: i18next.t(
                                "ui:toasts.protectionRequestFailed"
                            ),
                        });
                        Log.error(e);
                    });
            },
        },
        {
            id: "alertOnChange",
            icon: "notification_important",
            default: true,
            color: "var(--uv-icon-alertonchange-color, black)",
            visible: () => !UltravioletStore.isSpecialPage(),
            action() {
                Watch.toggle();
            },
        },
        {
            id: "latestRevision",
            icon: "watch_later",
            default: true,
            visible: () => !UltravioletStore.isSpecialPage(),
            action() {
                UltravioletStore.currentPage.navigateToLatestRevision();
            },
        },
    ];

    const nondefaultIcons = [
        {
            id: "vandalismStatistics",
            icon: "auto_graph",
            visible: () => true,
            action() {
                new UltravioletUI.IFrameDialog({
                    src: "https://redwarn.toolforge.org/tools/rpm/",
                    width: "90vw",
                }).show();
            },
        },
    ];

    const footerIcons = [
        {
            id: "preferences",
            icon: "settings",
            visible: () => true,
            action() {
                Page.fromTitle("Project:Ultraviolet/Preferences").navigate(
                    true
                );
            },
        },
        {
            id: "uvTalk",
            icon: "question_answer",
            visible: () => true,
            action() {
                redirect("https://w.wiki/54M7", true);
            },
        },
        {
            id: "copyLog",
            icon: "content_copy",
            visible: () => true,
            action() {
                copyToClipboard(btoa(JSON.stringify(Log.dump()))).then(
                    (res) => {
                        if (res) {
                            UltravioletUI.Toast.quickShow({
                                content: i18next.t("ui:toasts.logCopied"),
                            });
                        } else {
                            UltravioletUI.Toast.quickShow({
                                content: i18next.t("ui:toasts.copyFail"),
                            });
                        }
                    }
                );
            },
        },
    ];

    return [
        ...defaultUserIcons,
        ...getReportVenueIcons(),
        ...defaultIcons,
        ...nondefaultIcons,
        ...footerIcons,
        {
            // Always required.
            id: "moreOptions",
            icon: "more_vert",
            default: true,
            required: true,
            visible: () => true,
            action() {
                new UltravioletUI.ExtendedOptions().show();
            },
        },
    ];
};

export default PageIcons;
