import { Setting } from "app/config/user/Setting";
import UVUIElement, { UVUIElementProperties } from "./UVUIElement";

export interface UVUIPreferencesItemProperties extends UVUIElementProperties {
    name: string;
    setting: Setting<any>;
    onChange?: (value: any) => void;
}

export type UVUIPreferencesItemID = string;

export class UVUIPreferencesItem extends UVUIElement {
    public static readonly elementName = "uvPreferencesItem";

    /**
     * The HTMLDivElement which contains the item.
     */
    element?: HTMLDivElement;

    /**
     * The result of the item.
     */
    public result: any;

    constructor(readonly props: UVUIPreferencesItemProperties) {
        super();
        this.props = props;
    }

    /**
     * Renders the preferences item.
     */
    render(): HTMLDivElement {
        throw new Error("Attempted to call abstract method");
    }
}
