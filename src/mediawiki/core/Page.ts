import { MediaWikiAPI, Revision, User } from "app/mediawiki";
import UltravioletStore from "app/data/UltravioletStore";
import i18next from "i18next";
import {
    PageInvalidError,
    PageMissingError,
    SectionIndexMissingError,
} from "app/errors/MediaWikiErrors";
import { url as buildURL } from "app/util";
import redirect from "app/util/redirect";
import Section, { SectionContainer } from "app/mediawiki/core/Section";
import url from "app/util/url";
import UltravioletWikiConfiguration from "app/config/wiki/UltravioletWikiConfiguration";

export interface PageEditOptions {
    /**
     * The summary of the edit (without the Ultraviolet signature).
     */
    comment?: string;
    /**
     * The index of the section to edit, or "new" for a new section.
     */
    section?: string | number | Section;
    /**
     * Whether or not this text will be appended, be prepended, or replace the existing text.
     */
    mode?: "replace" | "append" | "prepend";
    /**
     * The basis revision of this page edit. This will help prevent possible edit conflicts.
     * Omit this parameter to edit a page regardless of content.
     */
    baseRevision?: Revision;
}

export interface PageLatestRevisionOptions {
    forceRefresh?: boolean;
    excludeUser?: User;
}

export interface PopulatedPage {
    pageID: number;
    namespace: number;
    title: mw.Title;
}

export interface NamedPage {
    title: mw.Title;
}

/**
 * A page is an object in a MediaWiki installation. All revisions stem from one page, and all
 * pages stem from one namespace. Since a `Page` object cannot be manually constructed, it must
 * be created using a page ID or using its title.
 */
export class Page implements SectionContainer {
    /** An index of all cached pages. **/
    private static pageIndex: Record<string, Page & NamedPage> = {};

    /** The ID of the page. */
    pageID?: number;

    /** The page title. */
    title?: mw.Title;

    /** The number that represents the namespace this page belongs to (i.e. its namespace ID). */
    namespace?: number;

    /** The latest revision cached by Ultraviolet. */
    latestCachedRevision?: Revision;

    /** The sections of this revision. */
    sections: Section[];

    /**
     * Get the latest revisions of a set of pages, and returns them all.
     *
     * <b>NOTE:</b> The order of the returned revisions may not reflect the order of the input pages.
     * @param pages The pages to get the latest revision of.
     * @param options Extra options or restrictions for getting the latest revision.
     * @param requestOptions Additional request options passed directly to the request.
     * @returns For each page with a valid revision, a {@link Revision}.
     */
    static async getLatestRevisions(
        pages: Page[],
        options?: {
            throwIfMissing?: boolean;
            followRedirects?: boolean;
        },
        requestOptions: Record<string, any> = {}
    ): Promise<Revision[]> {
        const identifiers: Partial<Record<"pageids" | "titles", string>> = {};
        if (pages.every((p) => p.pageID)) {
            identifiers.pageids = pages.map((p) => p.pageID).join("|");
        } else if (pages.every((p) => p.title)) {
            identifiers.titles = pages
                .map((p) => p.title.getPrefixedDb())
                .join("|");
        } else {
            // No choice but to split this into two requests.
            return [
                ...(await Page.getLatestRevisions(
                    pages.filter((p) => p.pageID),
                    options,
                    requestOptions
                )),
                ...(await Page.getLatestRevisions(
                    pages.filter((p) => p.title),
                    options,
                    requestOptions
                )),
            ];
        }

        const apiRevisions = await MediaWikiAPI.get({
            action: "query",
            format: "json",
            prop: "revisions",
            ...identifiers,
            ...{ redirects: options?.followRedirects ? 1 : undefined },
            rvprop: ["ids", "comment", "user", "timestamp", "size", "content"],
            rvslots: "main",
            ...requestOptions,
        });

        const revisions: Revision[] = [];
        for (const page of pages) {
            // The title serialization function used here must always match the one used
            // in the identifier call at the start of this function.
            let realTitle = page.title.getPrefixedDb();

            // `normalized` will only be present if the page was requested with a title.
            // In such a case, `page.title` will also be present.
            if (apiRevisions.query.normalized) {
                const normalized = apiRevisions.query.normalized.find(
                    (v: { from: string; to: string }) =>
                        //
                        v.from === realTitle
                );
                if (normalized) realTitle = normalized.to;
            }
            if (options.followRedirects && apiRevisions.query.redirects) {
                // Check if the page redirected to another.
                const redirect = apiRevisions.query.redirects.find(
                    (v: { from: string; to: string }) =>
                        // `v.from` will always be the normalized title.
                        v.from === realTitle
                );
                if (redirect) realTitle = redirect.to;
            }

            const identifier = page.getIdentifier();
            const apiPage = apiRevisions.query.pages.find((v: any) =>
                identifier instanceof mw.Title
                    ? v.title === realTitle
                    : (v.pageid = page.pageID)
            );

            if (apiPage.missing) {
                if (options?.throwIfMissing ?? true)
                    throw new PageMissingError({ page });
                else continue;
            } else if (apiPage.special) {
                throw new PageInvalidError({ page, reason: "Special page." });
            }

            const pageLatestRevision = apiPage.revisions[0];
            page.latestCachedRevision = Revision.fromPageLatestRevision(
                pageLatestRevision.revid,
                { query: { pages: [apiPage] } }
            );
            revisions.push(page.latestCachedRevision);
        }

        return revisions;
    }

    /**
     * Returns the URL of the page.
     */
    get url(): string {
        const identifier = this.getIdentifier();
        if (typeof identifier === "number")
            return buildURL(UltravioletStore.wikiIndex, { curid: identifier });
        else return this.articleURL;
    }

    /**
     * Gets this page's article URL (/wiki/Page)
     */
    get articleURL(): string {
        return UltravioletStore.articlePath(this.title.getPrefixedText());
    }

    private constructor(object?: Partial<Page>) {
        if (!!object) {
            Object.assign(this, object);
        }
    }

    /**
     * Creates a `Page` object from its ID. This is not suggested, as
     * it almost always needs extra population before actual usage.
     *
     * @param pageID The page's ID.
     */
    static fromID(pageID: number): Page {
        return new Page({ pageID: pageID });
    }

    /**
     * Creates a `Page` object from its title.
     * @param pageTitle The page's title (including namespace).
     */
    static fromTitle(pageTitle: string | mw.Title): Page & NamedPage {
        const mwTitle =
            typeof pageTitle == "string" ? new mw.Title(pageTitle) : pageTitle;
        return (
            Page.pageIndex[`${mwTitle}`] ??
            (Page.pageIndex[`${mwTitle}`] = <Page & NamedPage>new Page({
                title: mwTitle,
                namespace: mwTitle.namespace,
            }))
        );
    }

    /**
     * Creates a `Page` object from its title and ID. Best choice.
     * @param pageID The page's ID.
     * @param pageTitle The page's title (including namespace).
     */
    static fromIDAndTitle(
        pageID: number,
        pageTitle: string
    ): Page & PopulatedPage {
        const mwTitle =
            typeof pageTitle == "string" ? new mw.Title(pageTitle) : pageTitle;

        if (
            Page.pageIndex[`${mwTitle}`] &&
            Page.pageIndex[`${mwTitle}`].pageID == null
        )
            Page.pageIndex[`${mwTitle}`].pageID = pageID;

        return (
            <Page & PopulatedPage>Page.pageIndex[`${mwTitle}`] ??
            (Page.pageIndex[`${mwTitle}`] = <Page & PopulatedPage>new Page({
                pageID: pageID,
                title: mwTitle,
                namespace: mwTitle.namespace,
            }))
        );
    }

    static isSpecialPage(
        page: (Page & NamedPage) | (Page & { namespace: number })
    ): boolean;
    static isSpecialPage(page: Page): Promise<boolean>;
    /**
     * Determines whether or not this page is a userspace (either
     * User or User talk page).
     *
     * This is not configurable on most wikis unless completely
     * modified since MediaWiki ships with NS_USER and NS_USER_TALK
     * hardcoded as PHP constants.
     */
    static isSpecialPage(page: Page): PromiseOrNot<boolean> {
        if (page.isNamed() || page.namespace != null) {
            return (page.namespace || page.title.getNamespaceId()) < 0;
        } else {
            // If UV reaches this, something is clearly inefficient.
            // Populate `this.title`.
            return page
                .getLatestRevision()
                .then((revision) => Page.isSpecialPage(revision.page));
        }
    }

    static isUserspacePage(page: Page & NamedPage): "user" | "talk" | false;
    static isUserspacePage(page: Page): Promise<"user" | "talk" | false>;
    /**
     * Determines whether or not this page is a userspace (either
     * User or User talk page).
     *
     * This is not configurable on most wikis unless completely
     * modified since MediaWiki ships with NS_USER and NS_USER_TALK
     * hardcoded as PHP constants.
     */
    static isUserspacePage(page: Page): PromiseOrNot<"user" | "talk" | false> {
        if (page.isNamed()) {
            return (
                (page.namespace == 2 && "user") ||
                (page.namespace == 3 && "talk") ||
                false
            );
        } else {
            if (page.title == null)
                // Populate `this.title`.
                return page
                    .getLatestRevision()
                    .then((revision) => Page.isUserspacePage(revision.page));
        }
    }

    /**
     * Returns true if the page exists.
     * @param page The page to check.
     */
    static async exists(page: Page): Promise<boolean> {
        try {
            return (await Page.getLatestRevision(page)) != null;
        } catch (e) {
            if (e instanceof PageMissingError) return false;
            throw e;
        }
    }

    /**
     * Get a page's latest revision.
     * @param page The page to get the latest revision of.
     * @param options Extra options or restrictions for getting the latest revision.
     * @param requestOptions Additional request options passed directly to the request.
     * @returns `null` if no matching revisions were found. A {@link Revision} otherwise.
     */
    static async getLatestRevision(
        page: Page,
        options?: { excludeUser?: User },
        requestOptions: Record<string, any> = {}
    ): Promise<Revision> {
        // Returns one revision from one page (the given page).
        const revisionInfoRequest = await MediaWikiAPI.get({
            action: "query",
            format: "json",
            prop: "revisions",
            ...page.getAPIIdentifier(),
            rvprop: ["ids", "comment", "user", "timestamp", "size", "content"],
            rvslots: "main",
            rvexcludeuser: options?.excludeUser?.username ?? undefined,
            ...requestOptions,
        });

        if (revisionInfoRequest["query"]["pages"][0]) {
            if (!!revisionInfoRequest["query"]["pages"][0]["missing"])
                throw new PageMissingError({ page });
            if (!!revisionInfoRequest["query"]["pages"][0]["invalid"])
                throw new PageInvalidError({
                    page,
                    reason: revisionInfoRequest["query"]["pages"][0][
                        "invalidreason"
                    ],
                });
        }
        if (revisionInfoRequest["query"]["pages"][-1]) {
            throw new Error("Invalid page ID or title.");
        }

        const pageData: Record<string, any> = Object.values(
            revisionInfoRequest["query"]["pages"]
        )[0];

        // Fill in blank values from the page if available.
        if (!page.title) page.title = pageData["title"];
        if (!page.namespace) page.namespace = pageData["ns"];

        // No revisions found. Return null.
        if (!pageData["revisions"] || pageData["revisions"].length === 0)
            return null;

        // Title is always provided. IDs are required (see toPopulate declaration).
        return (page.latestCachedRevision = Revision.fromPageLatestRevision(
            pageData["revisions"][0]["revid"],
            revisionInfoRequest
        ));
    }

    isNamed(): this is NamedPage {
        return this.title != null;
    }

    /**
     * Grabs either the page's title or ID. Returns the title if both exist as long as
     * `favorTitle` is set to true.
     *
     * If this function returns `null`, the `Page` was illegally created.
     * @param favorTitle Whether or not to favor the title over the ID.
     */
    getIdentifier(favorTitle = true): number | mw.Title {
        if (!!this.pageID && !favorTitle) return this.pageID;
        else if (!this.pageID && !favorTitle) return this.title ?? null;
        else if (!!this.title && favorTitle) return this.title;
        else if (!this.title && favorTitle) return this.pageID ?? null;
    }

    /**
     * Get an object with all parameters required for a page when using the API's query module.
     */
    getAPIIdentifier(): Record<string, any> {
        const identifier = this.getIdentifier();
        return {
            [typeof identifier === "number"
                ? "pageids"
                : "titles"]: `${identifier}`,
        };
    }

    async exists(): Promise<boolean> {
        return Page.exists(this);
    }

    /**
     * Get a page's latest revision.
     */
    async getLatestRevision(
        options: PageLatestRevisionOptions = {}
    ): Promise<Revision> {
        const clean =
            Object.keys(options).filter((k) => k !== "forceRefresh").length ===
            0;
        if (
            options.forceRefresh ||
            this.latestCachedRevision == null ||
            !clean
        ) {
            if (clean) {
                this.latestCachedRevision = await Page.getLatestRevision(
                    this,
                    options
                );
            } else {
                return Page.getLatestRevision(this, options);
            }
        }

        return this.latestCachedRevision;
    }

    /**
     * Get the sections of this page's latest revision.
     */
    async getSections(): Promise<Section[]> {
        return Section.getSections(this);
    }

    async firstSection(): Promise<Section> {
        return Section.getSections(this).then((s) => s[0]);
    }

    async lastSection(): Promise<Section> {
        return Section.getSections(this).then((s) => s[s.length]);
    }

    async findSection(identifier: number | string): Promise<Section | null> {
        return Section.getSections(this).then((sections) => {
            return (
                sections.find((section) =>
                    typeof identifier === "string"
                        ? section.title === identifier
                        : section.index === identifier
                ) ?? null
            );
        });
    }

    /**
     * Checks if the page's latest revision has been cached.
     */
    hasLatestRevision(): boolean {
        return !!this.latestCachedRevision;
    }

    /**
     * Gets the latest revision of the page which was not by a given user.
     * @param user The user.
     */
    async getLatestRevisionNotByUser(user: User): Promise<Revision> {
        return this.getLatestRevision({
            excludeUser: user,
        });
    }

    /**
     * Navigate to the page.
     */
    navigate(newTab = false): void {
        redirect(this.url, newTab);
    }

    /**
     * Navigate to the given revision's diff page.
     */
    navigateToLatestRevision(): void {
        redirect(
            url(UltravioletStore.wikiIndex, {
                diff: 0,
                title: this.title,
            })
        );
    }

    /**
     * Opens the page in a new tab.
     */
    openInNewTab(): void {
        open(this.url);
    }

    /**
     * Gets a subpage of the given page.
     * @param subpage The subpage to get. Don't include the page's original title.
     * @returns The subpage requested.
     */
    getSubpage(subpage: string): Page {
        return Page.fromTitle(`${this.title.getPrefixedText()}/${subpage}`);
    }

    /**
     * Edit the page.
     *
     * @throws {Error}
     * @param content The new page content.
     * @param options Page editing options.
     */
    async edit(content: string, options: PageEditOptions): Promise<any> {
        const pageIdentifier = this.getIdentifier();

        // Handle the section
        /**
         * The {@link Section} to append text to.
         */
        let existingSection: Section = null;
        if (options.section) {
            // For `Section` objects.
            if (
                options.section instanceof Section &&
                options.section.revision === this.latestCachedRevision
            )
                // Use the existing section on this talk page.
                existingSection = options.section;
            else if (options.section instanceof Section)
                // Replace section option with section title for automatic correction.
                options.section = options.section.title;

            if (this.sections == null) await this.getSections();

            const revision = this.sections[0].revision;
            const revisionSections = this.sections;

            // Search for an existing section.
            if (existingSection == null) {
                if (!revision && typeof options.section === "number") {
                    // Immediate failure since a non-existent page has no sections.
                    throw new PageMissingError({ page: this });
                } else if (typeof options.section === "number") {
                    existingSection = revisionSections.filter(
                        (s) => s.index === options.section
                    )[0];

                    // Section not found. Hard fail since there's no fallback title.
                    if (existingSection == null)
                        throw new SectionIndexMissingError({
                            sectionId: options.section,
                            revision,
                        });
                } else {
                    existingSection = revisionSections.filter(
                        (s) => s.title === options.section
                    )[0];

                    // Section not found. It will be appended to the end of the user talk page.
                    // Remove leading whitespace to avoid extra spaces.
                    if (existingSection == null) content = content.trimStart();
                }
            }
        }

        // Handle the mode
        let textArgument: Record<string, any>;
        switch (options.mode) {
            case "append":
                textArgument = { appendtext: content };
                break;
            case "prepend":
                textArgument = { prependtext: content };
                break;
            default:
                textArgument = { text: content };
                break;
        }

        return MediaWikiAPI.postWithEditToken({
            action: "edit",
            format: "json",

            // Page ID or title
            [typeof pageIdentifier === "number"
                ? "pageid"
                : "title"]: `${pageIdentifier}`,

            // Edit summary
            summary: `${options.comment ?? ""} ${i18next.t(
                "common:ultraviolet.signature"
            )}`,

            // Tags
            tags: UltravioletWikiConfiguration.c.meta.tag,

            // Base revision ID
            ...(options.baseRevision
                ? {
                      baserevid: options.baseRevision.revisionID,
                  }
                : {}),

            // Section
            ...(options.section
                ? existingSection
                    ? {
                          section: existingSection.index,
                      }
                    : {
                          section: "new",
                          sectiontitle: options.section,
                      }
                : {}),

            ...textArgument,
        });
    }

    /**
     * Appends wikitext to the page.
     *
     * @param text The content to add.
     * @param options Page editing options.
     */
    async appendContent(
        text: string,
        options?: Omit<PageEditOptions, "mode">
    ): Promise<any> {
        // Force using append mode.
        return this.edit(
            text,
            Object.assign({ mode: <const>"append" }, options)
        );
    }

    /**
     * Prepends wikitext to the page.
     *
     * @param text The content to add.
     * @param options Page editing options.
     */
    async prependContent(
        text: string,
        options?: Omit<PageEditOptions, "mode">
    ): Promise<any> {
        // Force using prepend mode.
        return this.edit(
            text,
            Object.assign({ mode: <const>"prepend" }, options)
        );
    }
}
